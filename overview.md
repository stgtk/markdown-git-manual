# Übersicht

## Motivation

Gewisse Leser.innen fragen sich wohl, wieso sich die Auseinandersetzung mit einer Alternative von Word und Konsorten lohnenswert ist. Darum hier eine Liste der Vorteile:

- **Reiner Text.** [Markdown](https://de.wikipedia.org/wiki/Markdown) besteht aus reinem Text. Im Gegensatz also zu den Formaten von Word, Pages usw. können die Dateien auf jedem System ohne besonderes Programm geöffnet werden (auch auf Smartphones und Tablets). Auch ist sicher gestellt, dass es keine Unstimmigkeiten zwischen verschiedenen Programmen gibt. Abschließend vereinfachen Text Dateien die Archivierung und langfristige Verwaltung von Daten. Ein Faktum, welches gerade in der heutigen Zeit mit ständig wechselnden Programmen und immer neuen Innovationen wichtiger den je ist.
- **Trennung von Inhalt und Format.** Mit Markdown wird der Text von dessen Formatierung strikt getrennt. Beim Schreiben und Redigieren kann man sich so komplett auf den eigentlichen Inhalt konzentrieren. Aus der ursprünglichen Markdown-Datei können danach unterschiedlichste Endprodukte (PDF, Webseiten, Bücher usw.) entstehen.
- **Versionierung** Jede.r kennt die Schwierigkeiten, wenn länger an einem Text gearbeitet wird. Plötzlich gibt es die unterschiedlichsten Versionen als eigene Datei. Fast jede.r kennt die Situation, wenn am Ende der Inhalt des Projektordners etwa so aussieht: `arbeit.docx`, `arbeit_regidiert.docx`, `arbeit_finale-version_2.docx`. Genau diesem Problem nimmt sich [git](https://git-scm.com/) an. Ursprünglich stammt die Software aus der Softwareentwicklung, wo man ebenfalls mit der Problematik konfrontiert ist, eine grössere Menge von Textdateien stets auf der korrekten Version zu halten. Durch die Verwendung von git kann man zu jedem Zeitpunkt auf alle älteren Versionen zurückgreifen. 
- **Zusammenarbeit.** Auch im kreativen Bereich gehören Kollaborationen längst zum Alltag. Mit git ist es möglich mit mehreren Personen an einem Dokument zu arbeiten. Sollten Konflikte auftreten (zwei verschiedene Personen haben andere Teile eines Texts bearbeitet), gibt es komfortable Werkzeuge, welche das Zusammenfügen von verschiedenen Änderungen zu einem Kinderspiel machen. Auch kann jede Person auf verschiedenen Geräten arbeiten.
- **Backups** Durch die Funktionsweise von git ist sichergestellt, dass stets ein Backup aller Daten vorhanden ist.

## Aufbau

_Todo._
