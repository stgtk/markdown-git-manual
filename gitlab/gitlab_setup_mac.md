# Setup von GitLab (MacOS)


## Zweck

[GitLab](https://about.gitlab.com/) ist eine Plattform auf welcher sogenannte git repositories (also einzelne Projekte) gespeichert werden können und für alle zugänglich sind. Auch sind eine Vielzahl von hilfreichen Tools vorhanden, welche das gemeinsame Arbeiten an Projekten erleichtern. Alternativen sind etwa [GitHub](https://github.com/) aber auch [Gitea](https://gitea.io/) welches auf einem eigenen Server gehostet werden kann. Für den Anfang empfehlen wir aber einfach einen GitLab Account.


## Accounterstellung

Öffnen Sie die [Registrationsseite von GitLab](https://gitlab.com/users/sign_in#register-pane) und füllen Sie die geforderten Felder aus. Bitte beachten Sie, dass Sie für die gesamte Anleitung die selbe Mailadresse verwenden. Bestätigen Sie danach ihre Angaben mit einem Klick auf «Register».

![GitLab Registration](gitlab_setup_mac/01.png)

Sie werden danach aufgefordert, Ihren Mailadresse zu bestätigen. Öffnen Sie hierzu ihr Mail. Wenn Sie keine Nachricht von GitLab erhalten haben, schauen Sie ebenfalls in Ihrem Spam-Ordner nach. Wenn Sie die Mail geöffnet haben, bestätigen Sie Ihre Adresse mit einem Klick auf «Confirm your account».

![GitLab Mail](gitlab_setup_mac/02.png)

Sie können Sich danach [hier](https://gitlab.com/users/sign_in) einloggen.


## SSH Schlüssel (Optional)

Damit Sie sich nicht jedes Mal, wenn Sie Änderungen auf GitHub schreiben, sich einloggen müssen, können Sie einen SSH Key hinzufügen. Dieser Schritt ist Optional und kann auch an einem späteren Zeitpunkt nachgeholt werden.

Dieser Vorgang erfordert das Terminal. Öffnen Sie das Programm unter `Programme/Dienstprogramme/Terminal`.

![Terminal öffnen](gitlab_setup_mac/03.png)

Im Terminal geben sie nun folgenden Befehl ein: `ssh-keygen -o -t rsa -b 4096 -C "gitlab"` (Sie können diesen Befehl auch kopieren und im Terminal einfügen).

![SSH Keygen Command](gitlab_setup_mac/04.png)

Starten Sie nun das Programm, es wird Ihnen eine Reihe von Fragen stellen, welche Sie wie folgt beantworten können:

1. Bestätigen Sie nun die Eingabe mit `Enter (⏎)`
2. Wenn Sie keinen speziellen Grund haben, bestätigen Sie den Speicherort mit `Enter (⏎)`
3. Bestätigen Sie die Frage nach einem Passwort mit `Enter (⏎)`
4. Bestätigen Sie die Frage nach der Wiederholung des Passworts mit `Enter (⏎)`

![SSH Keygen Run](gitlab_setup_mac/05.png)

Sobald der SSH Schlüssel generiert wurde, muss dieser in die Zwischenablage kopiert werden. Tun Sie diesen mit folgendem Befehl: `pbcopy < ~/.ssh/id_rsa.pub` im Terminal, welcher Sie wiederum mit `Enter (⏎)` bestätigen.

![SSH Schlüssel kopieren](gitlab_setup_mac/06.png)

Nun muss der Schlüssel nur noch GitHub bekannt gegeben werden. Öffnen Sie hierzu die [entsprechende Einstellungsseite](https://gitlab.com/profile/keys). Fügen sie nun mit `cmd(⌘) + v` den Schlüssel in das grosse Textfeld ein und bestätigen Sie die Eingabe mit einem Klick auf «Add key».

![SSH Schlüssel konfigurieren](gitlab_setup_mac/07.png)

Wenn Sie eine Seite ähnlich zu der Unten sehen, haben Sie den Schlüssel erfolgreich konfiguriert.

![SSH Schlüssel erfolgreich konfiguriert](gitlab_setup_mac/08.png)

(Weitere Informationen über diesen Vorgang finden Sie in der [offiziellen Dokumentation](https://docs.gitlab.com/ee/ssh/).)


## Wie Weiter

- Weiter in der Installation: [Setup von Git (MacOS)](../git/git_setup_mac.md)
- Mehr über die Benutzung von GitLab: [Benutzung von GitLab](gitlab_usage.md)

