# Einführung in git

Für ein effizientes Arbeiten mit der Umgebung ist es sinnvoll, die grundsätzlichen Konzepte von git zu verstehen. Diese Anleitung verzichtet hierbei auf unnötige Details und Komplexität. Personen, welche mehr erfahren möchten, sei das englischsprachige [GitHowTo](https://githowto.com/) empfohlen. Noch mehr Informationen (in einer technischen Sprache) bietet die [offiziellen Dokumentation von git](https://git-scm.com/book/de/v1/Los-geht%E2%80%99s-Git-Grundlagen). Wer beim ersten Durchlesen nicht alles versteht, keine Angst! Durch den Einsatz von VSCodium sind die unten beschriebenen Schritte ganz einfach und ohne viel Nachdenken zu bewerkstelligen. Dennoch hilft es, das Konzept einmal gelesen zu haben.


## Repository und lokale Kopien

Befassen wir uns zunächst mit dem Konzept der Repository. Die Repository verbindet alle, welche an dem Projekt arbeiten. Im Grunde ist sie nichts anders als ein normaler Ordner. Damit nun verschiedene Menschen auf diesen Ordner zugreifen können, befinden sich diese Repos meist im Netz. In unserem Falle benutzen wir GitLab hierfür. Der Vorteil von GitLab ist, dass es neben den reinen Dateinspeicherung noch diverse andere Features bietet (mehr hierzu erfahren Sie im [nächsten Kapittel](../gitlab/gitlab_usage.md)).

Mithilfe von `clone` ist es nun möglich, sich eine lokale Kopie der Repo zu ziehen, welche man danach bearbeitet. Jede Person, welche sich an einem Projekt beteiligt, verfügt über ihre eigene lokale Kopie. Es ist natürlich auch möglich, dass eine Person mehrere lokale Kopien besitzt, dies ist insbesondere dann praktisch, wenn man mehr als einen PC besitzt. Sehen wir uns ein Fallbeispiel an.

Anna und Bob arbeiten an einem gemeinsamen Theaterprojekt, Hamlet. Sie wollen die Textentwicklung gemeinsam mit git koordinieren. Hierfür haben Sie auf GitLab eine neue Repository angelegt (das [nächsten Kapittel](../gitlab/gitlab_usage.md) wird erklären, wie man auf GitLab neue Repos anlegen kann). Die Situation sieht nun wie folgt aus:

![Repo on GitLab](git_introduction/01.png)

Nun brauchen die beiden aber eine lokale Kopie der Daten auf ihrem PC, damit sie mit dem Arbeiten beginnen können. Hierfür klonen (`clone`) sie jeweils den Ordner auf ihren Rechner. Sie verfügen nun beide über eine exakte Kopie aller Daten.

![Git clone](git_introduction/02.png)

Anna und Bob beginnen nun mit dem Schreiben. Wenn sie nun der Meinung sind, dass der andere das Ergebnis sehen soll, schieben Sie die Änderungen mit `push` wieder auf den GitLab Server (und hiermit in die Repository). Wenn Sie Änderungen des anderen holen möchten, benutzen sie `pull`. Wenn nun auf dem Server neue Änderungen bereit liegen, werden die zur lokalen Kopie hinzugefügt. Hierbei stellt git sicher, dass keine Änderungen verloren gehen.

![Git push and pull](git_introduction/03.png)


## Arbeiten mit der lokalen Kopie

Die Funktionsweise von git ist nicht mit der von Dropbox oder Nextcloud zu vergleichen. Denn git synchronisiert nicht einfach die Ordner, wie es die genannten Programme machen. Und dies hat einen triftigen Grund: Die Cloud basierten Datensynchronisationstools funktionieren gut bis zu dem Moment, wenn zwei Personen die gleiche Datei bearbeiten. Dann muss der Benutzer mühsam von Hand die verschiedenen Änderungen zusammen kopieren. Um genau dies zu verhindern, ist die Arbeitsweise bei git etwas anders. Auch wenn diese aufs Erste etwas komplexer anmutet, so wollen es die Meisten nach kurzer Zeit nicht mehr missen.

Die lokale Kopie der Repo besteht bei git aus drei Stufen, welche wir nachfolgend erläutern werden:

**Die Arbeitskopie.** Die Arbeitskopie ist nichts anderes, als der Ordner, welcher Sie sich mit `clone` von GitLab heruntergeladen haben. 

**Der Index.** Auch `stage` genannt. Für jede Arbeitskopie bzw. Repo führt git einen Index mit den Daten, welche von git berücksichtigt werden. Die Idee dahinter ist folgende: Oft legt man irgendwelche Dateien an und merkt kurze Zeit später, dass man Sie gar nicht braucht. Da diese danach meist nicht gelöscht werden, treiben sie danach für alle sichtbar als «Datenleiche» in den Projektordnern umher. Damit dies nicht geschieht, muss jede Datei einmal mit einem `add` auf den Index geschrieben werden. Dateien, welche nicht auf diesem stehen, werden bei einem `push` nie auf den GitLab Server hochgeladen, verbleiben aber in der lokalen Kopie vorhanden.

Weiter verzeichnet git auf diesem Index alle lokalen Änderungen an Dokumenten. Wird eine Datei bearbeitet, so merkt dies git automatisch und verzeichnet diese Änderungen im Index. Dieser Vorgang nennt sich auch `staging`. `stage` bezeichnet also alle Änderungen, welche aktuell gemacht wurden.

**HEAD.** Der HEAD Umfasst alle Änderungen, welche beim nächsten `push` auf den Server geschrieben werden sollen. Um Änderungen im Index (also Veränderungen in Daten, welche git bekannt sind) auf den HEAD zu schreiben, wird `commit` verwendet. Für jeden Commit wird auch eine kurze Nachricht erwartet, welche die Änderungen erläutert. Diese Nachrichten haben sich auch in der künstlerischen Praxis als äusserst hilfreich erwiesen, da immer klar ist, wer was warum verändert hat. Durch git kann man auch zu jeder Zeit den Zustand nach jedem Commit zurückgreifen. Dies ist etwa hilfreich, wenn «verschwundener» Text gesucht wird, oder gewisse Änderungen wieder rückgängig gemacht werden müssen.

Mit `commit` kann man auch nur ausgewählte Dateien zum HEAD hinzufügen. Des Weiteren sammelt git die Commits, das bedeutet, mit einem `push` können auch mehrere Commits hochgeladen werden.

![Workflow](git_introduction/04.png)


## Workflow

Nach dem nun Gelernten, läuft der Workflow wie folgt ab:

1. Repository auf GitLab anlegen.
2. Die Repository klonen (`clone`).
3. Wenn eine neue Datei angelegt wird, diese mit `add` dem Index hinzufügen.
4. Mit `commit` Änderungen im Index zum HEAD hinzufügen und kommentieren.
5. Mit `push` die Änderungen im HEAD auf den Server schieben.
6. Mit `pull` Änderungen von Anderen vom Server holen.

Im Alltag braucht man meist nur die Schritte 3. bis 6.


## Konflikte


## Einschränkungen

