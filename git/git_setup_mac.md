# Setup Git (MacOS)

## Zweck

[Git](https://git-scm.com/) ermöglicht die Versionsverwaltung, das gemeinsame Arbeiten an Dateien und die Integration in GitLab.


## Installation

Laden Sie zunächst git von der [offiziellen Seite](https://git-scm.com/download/mac) herunter. Klicken Sie hierzu auf «click here to download manually». Sie werden auf die Seite SourceForge weitergeleitet, wo dann der Download startet.

![Git herunterladen](git_setup_mac/01.png)

Sobald Sie die Datei herunter geladen haben, öffnen Sie diese in den Downloads. Beispielhaft hier Safari:

![Git Image öffnen](git_setup_mac/02.png)

Da die Installationsdatei nicht aus dem App Store stammt, muss die Installation wie folgt aufgerufen werden: Klicken Sie bei **gehaltener** `ctrl` Taste auf die Installationsdatei und führen Sie diese danach mit einem Klick auf «Öffnen» aus.

![Installation starten](git_setup_mac/03.png)

Klicken Sie auf «Öffnen».

![Installation öffnen](git_setup_mac/04.png)

Klicken Sie auf «Fortfahren».

![Installation fortfahren](git_setup_mac/05.png)

Klicken Sie auf «Installieren».

![Installation Installieren](git_setup_mac/06.png)

Geben Sie ihr Passwort von Ihrem Benutzerkonto ein.

![Passworteingabe](git_setup_mac/07.png)

Die Installation startet nun. Warten Sie, bis diese erfolgreich abgeschlossen wurde. Schließen Sie diese mit einem Klick auf «Schließen».

![Installation abschliessen](git_setup_mac/08.png)


## Konfiguration

Git muss mit dem Username und der Mailadresse des GitLab-Accounts konfiguriert werden. Dieser Vorgang erfordert das Terminal. Öffnen Sie das Programm unter `Programme/Dienstprogramme/Terminal`.

![Terminal öffnen](git_setup_mac/09.png)

Geben Sie im Terminal folgenden den Befehl, `git config --global user.name "<USERNAME>"`, ein, wobei sie `<USERNAME>` durch Ihren GitLab-Benutzername ersetzen. Bestätigen Sie den Befehl mit `Enter (⏎)`.

![Git Benutzername setzen](git_setup_mac/10.png)

Geben Sie danach im Terminal folgenden den Befehl, `git config --global user.email "<MAIL>"`, ein, wobei sie `<MAIL>` durch Ihre Mailadresse ersetzen, welche Sie bei GitLab angegeben haben. Bestätigen Sie den Befehl mit `Enter (⏎)`.

![Git Mail setzen](git_setup_mac/11.png)

Hiermit ist die Installation und Konfiguration von git abgeschlossen.

## Wie Weiter

- Weiter in der Installation: [Setup von VSCodium (MacOS)](../vscodium/vscodium_setup_mac.md)
