# VSCodium Benutzen

[VSCodium](https://vscodium.com/) ist im Grunde nichts anderes als ein erweiterbarer Texteditor welcher auf [VSCode](https://code.visualstudio.com/) von Microsoft basiert. Der einzige Unterschied besteht darin, dass in VSCodium die proprietären Module von Microsoft entfernt sind und die Telemetrie, also das «Nachhauseschicken» von Daten, deaktiviert ist. Wenn Sie also mal etwas zu den Funktionen von VSCodium wissen wollen, verwenden Sie als Suchbegriff «VSCode». Alles was Sie finden werden, lässt sich auf beide Editoren anwenden.

Das Ziel dieser Anleitung ist es nicht, sämtliche Funktionen von VSCodium zu erläutern, da das Programm in vielerlei Hinsicht selbsterklärend ist. Vielmehr werden wir in Folge auf die Funktionen eingehen, welche aus anderen Programmen nicht bekannt ist und der Umgang mit git.

Diese Anleitung geht davon aus, dass Sie VSCodium unserer Anleitung entsprechend installiert und konfiguriert haben.


## Die Befehlspalette

Damit Sie die Aktionen nicht mühselig in den verschachtelten Menüs suchen müssen, bietet VSCodium die _Befehlspalette_ an. Mit dieser können Sie einfach nach Aktionen suchen und diese ausführen. Aufgrund dieses Vorteils arbeiten wir auch in dieser Anleitung mit dieser Palette. Sie können diese wie folgt aufrufen:

- **Windows, Linux:** `ctrl` + `shift (⇪)` + `p`
- **MacOS:** `cmd (⌘)` + `shift (⇪)` + `p`

Danach können Sie einen Befehlsnamen eintippen und diesen mit `enter (⏎)` ausführen.


## Git

Wir empfehlen, dass Sie vor diesem Abschnitt sich schon mit der [Philosophie und Funktionsweise von git](../git/git_introduction.md) vertraut haben.


### Clone

Zunächst müssen Sie die Repository auf Ihren Rechner bringen. Öffnen Sie hierfür in Ihrem Browser das gewünschte Projekt auf GitLab.

Klicken Sie dann auf «Clone» (1) und kopieren danach mit einem Klick auf den Button neben dem Textfeld den entsprechenden Link. Wenn Sie bei GitLab einen SSH-Key hinterlegt haben, wählen Sie die URL bei 2a (SSH-URL), sonst die URL bei 2b (HTTPS-URL). Beides ist möglich, der Vorteil bei der Verwendung von SSH ist, dass Sie nicht immer wieder Ihr Passwort eingeben müssen.

![GitLab clone link](vscodium_introduction/01.png)

Öffnen Sie nun VSCodium und rufen Sie die Befehlspalette auf (Windows, Linux: `ctrl` + `shift (⇪)` + `p`, Mac: `cmd (⌘)` + `shift (⇪)` + `p`), tippen Sie `git clone` und bestätigen Sie mit `enter (⏎)`.

![VSCodium clone cmd](vscodium_introduction/02.png)

In dem nächsten Dialog fügen Sie nun die kopierte URL aus GitLab ein und bestätigen Sie mit `enter (⏎)`.

![VSCodium enter git url](vscodium_introduction/03.png)

Sie werden nun aufgefordert, einen Speicherort für ihr Projekt zu wählen. VSCodium wird an diesem Ort ein neuer Ordner anlegen, welcher alle Daten des Projekts enthält.
