# Setup von VSCodium (MacOS)

## Zweck

[VSCodium](https://vscodium.com/) ist der verwendete Texteditor. Im Gegensatz zu dem, von MacOS mitgelieferten Editor, verfügt VSCodium über eine grosse Anzahl an Features, welche das Bearbeiten von Dokumenten und die Versionsverwaltung massgeblich vereinfachen.


## Installation

Öffnen Sie die [Download Seite](https://github.com/VSCodium/vscodium/releases) von VSCodium. Die Suche nach dem geeigneten Download gestaltet sich auf dieser Seite oft als etwas schwierig. Es gilt die Datei mit dem Namen `VSCodium.<VERSION>.dmg` zu finden, welche die höchste Versionsnummer aufweist. Die Dateien sind jeweils nach den einzelnen Veröffentlichungen («Releases») geordnet. Wenn die neuste Release keine Datei mit dem geforderten Namen aufweist, können bei einer älteren Version die Dateien mit einem Klick auf «Assets» aufgelistet werden.

![Download VSCodium](vscodium_setup_mac/01.png)

Da der Download nicht aus Apples App Store stammt, ist das erste Öffnen des Programms etwas umständlich. Konkret bedeutet dies, dass die Datei nicht direkt aus dem Browser geöffnet werden kann. Um dieses Problem zu umgehen, gehen Sie wie folgt vor (Beispielhaft für Safari, das Vorgehen in den anderen Browsers ist ähnlich):

1. Öffnen Sie die Liste der Downloads
2. Öffnen Sie das Kontextmenü mit einem **Rechts**klick auf die VSCodium Datei
3. Klicken Sie auf «Im Finder anzeigen»

![Finder öffnen](vscodium_setup_mac/02.png)

Klicken Sie nun bei **gehaltener** `ctrl` Taste auf die VSCodium Installationsdatei und führen Sie diese danach mit einem Klick auf «Öffnen» aus.

![Finder öffnen](vscodium_setup_mac/03.png)

Bestätigen Sie den die Rückfrage mit «Öffnen».

![Finder öffnen](vscodium_setup_mac/04.png)

1. Ziehen Sie die VSCodium-Applikation in den Applikations-Ordner
2. Öffnen Sie nun den Applikations-Ordner mit einem Doppelklick

![Finder öffnen](vscodium_setup_mac/05.png)

Ebenfalls muss die Applikation beim ersten Mal wie folgt gestartet werden: Klicken Sie nun bei **gehaltener** `ctrl` Taste auf die VSCodium Applikation und führen Sie diese danach mit einem Klick auf «Öffnen» aus.

![Finder öffnen](vscodium_setup_mac/06.png)

Bestätigen Sie den die Rückfrage mit «Öffnen».

![Finder öffnen](vscodium_setup_mac/07.png)


## Konfiguration

Um VSCodium für Markdown nutzen zu können, muss ein Plugin installiert werden. Hierfür gehen Sie wie folgt vor:

1. Öffnen Sie den Plugin Reiter mit einem Klick auf den entsprechenden Button auf der linken Seite
2. Geben Sie im Suchfeld den Begriff `markdown` ein und starten Sie die Suche mit `Enter (⏎)`
3. Klicken Sie auf das Suchergebnis «Markdown All in One»
4. Starten Sie die Installation des Plugins mit einem Klick auf «Install»

![Finder öffnen](vscodium_setup_mac/08.png)


## Deutsche Oberfläche

Wenn Sie sich eine deutschsprachige Benutzeroberfläche in VSCodium wünschen, führen Sie folgende Schritte aus:

1. Suchen Sie nach dem Plugin `german`
2. Wählen sie das Suchergebnis «German Language Pack for Visual Studio Code» aus
3. Installieren Sie das Plugin mit einem Klick auf «Install»

![Finder öffnen](vscodium_setup_mac/09.png)

Wenden Sie die Änderung der Oberfläche an, indem Sie auf «Restart Now» klicken.

![Finder öffnen](vscodium_setup_mac/10.png)
