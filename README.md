# Einleitung

Das kreative Arbeiten erfordert oft das Schreiben von Text. Auch wenn die Meisten, welche regelmäßig Texte verfassen, dies gerne tun, so ist nicht zu leugnen, dass die Formatierung und die Zusammenarbeit mit anderen Personen schnell von der eigentlichen Tätigkeit ablenken und die Arbeit unnötig erschweren. Mit dieser Anleitung wollen wir eine Alternative zu den üblichen Programmen wie Word bieten und aufzeigen, wie man sich mit einfachen Tools und Techniken wieder auf das Wesentliche konzentrieren kann: Den Text.

**Aufbau:** Zunächst beschäftigt sich die Anleitung mit der grundsätzlichen Installation der Tools. Aufbauend darauf, wird die Verwendung der Programme erläutert. Abschließend werden für Interessierte weiterführende Themen erläutert.

**Über die Autor.innen:** [Stgtk](https://stgtk.ch/) ist sich als Theaterproduktionskollektiv ein Zusammenschluss von Theaterschaffenden mit dem Ziel, die Produktionsbedingungen für seine Mitglieder zu vereinfachen. Weiter wollen sie Dritte mit Wissen, Material und Erfahrung unterstützten. Das [Solutionsbüro Bern](https://buero.io/) bietet für Kulturinstitutionen, kunst- und geisteswissenschaftlichen Organisationen oder Kunstschaffenden eine breite Palette an Dienstleistungen im Bereich Elektronik sowie Soft- und Hardware an.

# Anleitungen

Grundsätzliche Informationen über die Motivation und die einzelnen Komponenten finden sich [hier](overview.md). Da sich die Installation der einzelnen Komponenten zwischen den einzelnen Betriebssystemen stark unterscheidet, existiert für jedes System eine eigene Anleitung. 


## Installation

Bitte folgen sie den Links im Abschnitt für Ihr Betriebssystem.

### MacOS

- [Setup von GitLab (MacOS)](gitlab/gitlab_setup_mac.md)
- [Setup von Git (MacOS)](git/git_setup_mac.md)
- [Setup von VSCodium (MacOS)](vscodium/vscodium_setup_mac.md)

### Windows

_Todo._


## Benutzung

- [Einführung git](git/git_introduction.md)
- GitLab (Verwalten von Projekten)
- [VSCodium](vscodium/vscodium_usage.md)
- Markdown
- PDF Export


## Fortgeschrittenes

Diese Anleitungen sind für Interessierte, welche noch mehr lernen wollen, ihr Verständnis ist aber nicht notwendig.

- Git in der Kommandozeile
- Export mit Pandoc (PDF etc.)
- PDF Export mit gosmooth


# Lizenz

Alle Dokumente dieses Projekts sind unter der [Creative Commons Lizenz, Namensnennung bei Weitergabe unter gleichen Bedingungen 4.0](https://creativecommons.org/licenses/by-sa/4.0/deed.de) lizenziert. Weitere Informationen finden Sie unter dem Link oder [hier](LICENSE)

