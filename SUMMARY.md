## Übersicht

- [Einleitung](README.md)
- [Motivation und Übersicht](overview.md)

## Installation

### MacOS

- [Setup von GitLab (MacOS)](gitlab/gitlab_setup_mac.md)
- [Setup von Git (MacOS)](git/git_setup_mac.md)
- [Setup von VSCodium (MacOS)](vscodium/vscodium_setup_mac.md)

## Benutzung

- [Einführung git](git/git_introduction.md)
- [VSCodium](vscodium/vscodium_usage.md)
